#!/bin/bash

set -eu

log() {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    echo -e "\n# $1\n"
    set "-$prev_shell_config"
}

download_if_missing() {
    if [ -f "$1" ] && [ -s "$1" ]; then
        echo "Re-using the existing $1 artifact"
    else
        echo -e "\nDownloading $1"
        wget -O "$1" $2
    fi
}

# Interface selection
INTERFACE_INTERNET=$(ip route show default | awk '/default/ { print $5 }')
INTERFACES_AVAILABLE=$(ip -details -json link show | jq -r '
.[] |
      if .linkinfo.info_kind // .link_type == "loopback" then
          empty
      else
          .ifname
      end
' | grep -v $INTERFACE_INTERNET)

if [ "$#" -eq 1 ]; then
    INTERFACE_LISTEN=$1
elif [ $(echo "$INTERFACES_AVAILABLE" | wc -l) -eq 1 ]; then
    INTERFACE_LISTEN=$INTERFACES_AVAILABLE
else
    echo "ERROR: Found multiple candidate interfaces for the dnsmasq interface"
    echo ""
    echo "Usage: $0 [$(echo $INTERFACES_AVAILABLE | xargs | sed -e 's/ /|/g')]"
    echo ""

    exit 1
fi
echo "Exposing the netboot service on ${INTERFACE_LISTEN}, NATed to ${INTERFACE_INTERNET}"

LATEST_B2C_RELEASE=$(curl https://gitlab.freedesktop.org/api/v4/projects/20818/releases  2> /dev/null | jq -r '.[0].tag_name')
echo "Using Boot2container $LATEST_B2C_RELEASE"

# Downloading all the iPXE artifacts
log "Downloading/re-using the iPXE artifacts"
ARCH_ARM64=tftp/boot/arm64
ARCH_X86_64=tftp/boot/x86_64
download_if_missing $ARCH_X86_64/ipxe.efi https://boot.ipxe.org/ipxe.efi
download_if_missing $ARCH_X86_64/undionly.kpxe https://boot.ipxe.org/undionly.kpxe
download_if_missing $ARCH_ARM64/ipxe.efi https://boot.ipxe.org/arm64-efi/ipxe.efi

# Download all the boot2container artifacts
log "Downloading/re-using the boot2container artifacts"
while read URL_FILENAME ARCH_PATH NAME; do
    URL="https://gitlab.freedesktop.org/gfx-ci/boot2container/-/releases/${LATEST_B2C_RELEASE}/downloads/${URL_FILENAME}"
    LOCAL_FILENAME="b2c_${LATEST_B2C_RELEASE}_$NAME"
    LOCAL_PATH="$ARCH_PATH/$LOCAL_FILENAME"
    LATEST_SYMLINK_PATH="$ARCH_PATH/b2c_latest_$NAME"

    # Download the binaries if missing
    download_if_missing $LOCAL_PATH $URL

    # Update the symlink
    rm -f "$LATEST_SYMLINK_PATH" && ln -s "$PWD/$ARCH_PATH/$LOCAL_FILENAME" $LATEST_SYMLINK_PATH
done <<'EOD'
  linux-arm64                     tftp/boot/arm64        linux
  linux-arm64.firmware.cpio.xz    tftp/boot/arm64        firmware.cpio.xz
  linux-arm64.modules.cpio.xz     tftp/boot/arm64        modules.cpio.xz
  initramfs.linux_arm64.cpio.xz   tftp/boot/arm64        initramfs.cpio.xz
  linux-x86_64                    tftp/boot/x86_64       linux
  linux-x86_64.firmware.cpio.xz   tftp/boot/x86_64       firmware.cpio.xz
  linux-x86_64.modules.cpio.xz    tftp/boot/x86_64       modules.cpio.xz
  initramfs.linux_amd64.cpio.xz   tftp/boot/x86_64       initramfs.cpio.xz
EOD

log "Setting up the network interface $INTERFACE_LISTEN"

set -x
sudo ip addr flush dev $INTERFACE_LISTEN
sudo ip addr add dev $INTERFACE_LISTEN 10.0.0.1/24 || /bin/true

log "Routing traffic from the netbooted machines through INTERFACE_LISTEN $INTERFACE_INTERNET"

sudo sysctl -w net.ipv4.ip_forward=1 2> /dev/null
sudo iptables -A FORWARD -i $INTERFACE_LISTEN -o $INTERFACE_INTERNET -j ACCEPT
sudo iptables -A FORWARD -i $INTERFACE_LISTEN -o $INTERFACE_INTERNET -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o $INTERFACE_INTERNET -j MASQUERADE

log "Starting DNSMASQ"
sudo killall dnsmasq || true
sudo dnsmasq --port 0 \
  --dhcp-leasefile=$PWD/dnsmasq.leases \
  --dhcp-match=set:ipxe,175 \
  --dhcp-match=set:uboot-arm64,option:client-arch,22 \
  --dhcp-match=set:efi-arm64,option:client-arch,11 \
  --dhcp-match=set:efi-x86_64,option:client-arch,9 \
  --dhcp-match=set:efi-x86_64,option:client-arch,7 \
  --dhcp-match=set:efi-x86,option:client-arch,6 \
  --dhcp-match=set:pcbios,option:client-arch,0 \
  --tag-if=set:uboot-arm64-boot,tag:uboot-arm64,tag:!ipxe \
  --tag-if=set:efi-arm64-boot,tag:efi-arm64,tag:!ipxe \
  --tag-if=set:efi-x86_64-boot,tag:efi-x86_64,tag:!ipxe \
  --tag-if=set:efi-x86-boot,tag:efi-x86,tag:!ipxe \
  --tag-if=set:pcbios-boot,tag:pcbios,tag:!ipxe \
  --dhcp-boot=tag:uboot-arm64-boot,/ \
  --dhcp-boot=tag:efi-arm64-boot,/boot/arm64/ipxe.efi \
  --dhcp-boot=tag:efi-x86_64-boot,/boot/x86_64/ipxe.efi \
  --dhcp-boot=tag:pcbios-boot,/boot/x86_64/undionly.kpxe \
  --dhcp-boot=tag:ipxe,/boot/main.ipxe \
  --dhcp-range=10.0.0.10,10.0.0.100 \
  --dhcp-option=option:dns-server,9.9.9.9 \
  --dhcp-option=66,"10.0.0.1" \
  --dhcp-script=/bin/echo \
  --enable-tftp=$INTERFACE_LISTEN \
  --tftp-root=$PWD/tftp \
  --log-facility=- \
  --log-queries=extra \
  --conf-file=/dev/null \
  --interface=$INTERFACE_LISTEN \
  --log-dhcp \
  --no-daemon

# If you want to add debug information
# --log-dhcp \
# --log-debug \
