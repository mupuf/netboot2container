# Netboot2container

A quick and dirty solution to run your favourite containers on your local machines through the magic of netbooting, and
[boot2container](https://gitlab.freedesktop.org/mupuf/boot2container)!

[[_TOC_]]

## Requirements

### Network topology

```mermaid
graph TD
    I[Internet] <-->|eth0| G(Gateway)
    G <--> |eth1| S(Switch)
    S <--> U1[Raspberry<br>Pi 4]
    S <--> U2[Raspberry<br>Pi 3]
    S <--> U4[AMD64<br>Computer]
```

### Host / Gateway

Here are the list of packages you need to install before you may:

 * dnsmasq
 * ip
 * iptables
 * jq
 * sudo
 * wget

When this is done, just run the following command:

<details><summary>$ ./start.sh [interface]</summary>

    Exposing the netboot service on enp5s0f1u3, NATed to enp4s0
    Using Boot2container v0.9.9

    # Downloading/re-using the iPXE artifacts

    Re-using the existing tftp/boot/x86_64/ipxe.efi artifact
    Re-using the existing tftp/boot/x86_64/undionly.kpxe artifact
    Re-using the existing tftp/boot/arm64/ipxe.efi artifact

    # Downloading/re-using the boot2container artifacts

    Re-using the existing tftp/boot/arm64/b2c_v0.9.9_linux artifact
    Re-using the existing tftp/boot/arm64/b2c_v0.9.9_initramfs.cpio.xz artifact
    Re-using the existing tftp/boot/x86_64/b2c_v0.9.9_linux artifact
    Re-using the existing tftp/boot/x86_64/b2c_v0.9.9_initramfs.cpio.xz artifact

    # Setting up the network interface enp5s0f1u3

    + sudo ip addr flush dev enp5s0f1u3
    + sudo ip addr add dev enp5s0f1u3 10.0.0.1/24
    + log 'Routing traffic from the netbooted machines through INTERFACE_LISTEN enp4s0'

    # Routing traffic from the netbooted machines through INTERFACE_LISTEN enp4s0

    + sudo sysctl -w net.ipv4.ip_forward=1
    net.ipv4.ip_forward = 1
    + sudo iptables -A FORWARD -i enp5s0f1u3 -o enp4s0 -j ACCEPT
    + sudo iptables -A FORWARD -i enp5s0f1u3 -o enp4s0 -m state --state ESTABLISHED,RELATED -j ACCEPT
    + sudo iptables -t nat -A POSTROUTING -o enp4s0 -j MASQUERADE
    + log 'Starting DNSMASQ'

    # Starting DNSMASQ

    + sudo dnsmasq --port 0 --dhcp-leasefile=/home/mupuf/src/pxe_boot/dnsmasq.leases --dhcp-match=set:ipxe,175 --dhcp-match=set:efi-arm64,option:client-arch,11 --dhcp-match=set:efi-x86_64,option:client-arch,9 --dhcp-match=set:efi-x86_64,option:client-arch,7 --dhcp-match=set:efi-x86,option:client-arch,6 --dhcp-match=set:pcbios,option:client-arch,0 '--tag-if=set:efi-arm64-boot,tag:efi-arm64,tag:!ipxe' '--tag-if=set:efi-x86_64-boot,tag:efi-x86_64,tag:!ipxe' '--tag-if=set:efi-x86-boot,tag:efi-x86,tag:!ipxe' '--tag-if=set:pcbios-boot,tag:pcbios,tag:!ipxe' --dhcp-boot=tag:efi-arm64-boot,/boot/arm64/ipxe.efi --dhcp-boot=tag:efi-x86_64-boot,/boot/x86_64/ipxe.efi --dhcp-boot=tag:pcbios-boot,/boot/x86_64/undionly.kpxe --dhcp-boot=tag:ipxe,/boot/main.ipxe --dhcp-range=10.0.0.10,10.0.0.100 --dhcp-option=option:dns-server,9.9.9.9 --dhcp-script=/bin/echo --enable-tftp=enp5s0f1u3 --tftp-root=/home/mupuf/src/pxe_boot/tftp --log-facility=- --log-queries=extra --conf-file=/dev/null --interface=enp5s0f1u3 --no-daemon
    Jan 24 19:05:59 dnsmasq[375779]: started, version 2.88 DNS disabled
    Jan 24 19:05:59 dnsmasq[375779]: compile time options: IPv6 GNU-getopt DBus no-UBus i18n IDN2 DHCP DHCPv6 no-Lua TFTP conntrack ipset nftset auth cryptohash DNSSEC loop-detect inotify dumpfile
    Jan 24 19:05:59 dnsmasq-dhcp[375779]: DHCP, IP range 10.0.0.10 -- 10.0.0.100, lease time 1h
    Jan 24 19:05:59 dnsmasq-tftp[375779]: TFTP root is /home/mupuf/src/pxe_boot/tftp
</details>

## Netbooted machines

Test machines require:

 - An ethernet port, connected to the gateway's interface
 - Bootloader:
   - EFI-capable: Not a problem for x86_64, harder for aarch64 (see [arm64 bootloaders](https://gitlab.freedesktop.org/mupuf/boot2container/-/wikis/devices/arm64/bootloaders))
   - PXE-capable

Turn on the machine, and it should netboot!
