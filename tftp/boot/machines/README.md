# Per-machine configuration

To configure the boot configuration for your machine, please create a file
named `${MACADDRESS}.ipxe` that will contain an iPXE script setting all the
variables you may want. Example:

```sh
$ cat 60:be:b4:06:13:34.ipxe
#!ipxe

set hostname jsl_nuc
set cache_device auto
set swap 1G
set b2c_run -ti docker://archlinux:latest
```

Check out the [x86_64/default.ipxe](tftp/boot/x86_64/default.ipxe) or
[arm64/default.ipxe](tftp/boot/arm64/default.ipxe) to figure out which options
are available.
